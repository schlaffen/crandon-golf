var pars = [5, 4, 3, 5, 4, 3, 4, 3, 4, 5, 4, 3, 4, 5, 4, 4, 3, 5];
var currentHole = 0;
var worstPar = 7;

function setPlayers()
{
    var numberOfPlayers=
	document.getElementById("playerCount").elements[0].value;
    document.getElementById("krauts").innerHTML = "I was supposed to practice my CSS, but then I got sidetracked by " + numberOfPlayers + " Kraut(s).";
    document.getElementById("playerCount").elements[1].disabled = true;
    displayForms(numberOfPlayers);
}

function displayForms(numberOfPlayers)
{
    document.getElementById("holeCount").innerHTML="Scores for hole # " + 
	(currentHole + 1) + " (par " + pars[currentHole] + ")";
    if(pars[currentHole++] === 5) worstPar = 8;
    else worstPar = 7;
    var f = document.createElement("form");
    var i = document.createElement("input");
    i.setAttribute('type',"number");
    i.setAttribute('min',"1");
    i.setAttribute('max', worstPar);
    var b = document.createElement("input");
    b.setAttribute('type', "button");
    f.appendChild(i);
    for(i = 0; i < numberOfPlayers; i++)
    document.getElementsByTagName('body')[0].appendChild(f);
}



